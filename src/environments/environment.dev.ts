export const environment = {
  production: false,
  development: true,
  apiUrl: 'http://localhost:3000/v1/',
};
