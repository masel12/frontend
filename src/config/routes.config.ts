export const ROUTES = {
    '404': {
        path: '404',
        route: '/404',
        name: '404'
    },
    'workflow': {
        path: 'workflow',
        route: '/workflow',
        name: 'workflow'
    },
    'notification': {
        path: 'notification',
        route: '/notification',
        name: 'notification'
    },
    'email': {
        path: 'email',
        route: '/email',
        name: 'email'
    },
    'saved-forms': {
        path: 'saved-forms',
        route: '/saved-forms',
        name: 'saved-forms'
    },
    'stand-by': {
        path: 'stand-by',
        route: '/stand-by',
        name: 'stand-by'
    }
}
