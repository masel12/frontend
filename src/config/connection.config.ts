import { environment } from "../environments/environment.dev";

export class Config {
    private static url = environment.apiUrl;

    public static get api(): any {
        const saveForm_url: string = this.url + 'saveForm';
        const email_url: string = this.url + 'emails';
        const notification_url: string = this.url + 'notifications';

        return {
            // Saved Forms
            savedForm: {
                create: saveForm_url
            },

            // Email
            email: {
                get: email_url,
                save: email_url + '/create'
            },

            // Notification
            notification: {
                get: notification_url,
                save: notification_url + '/create'
            }
        }
    }
}
