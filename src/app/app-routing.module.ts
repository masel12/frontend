import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTES } from '../config';
import { EmailComponent } from './components/email/email.component';
import { NotificationComponent } from './components/notification/notification.component';
import { StandByComponent } from './components/stand-by/stand-by.component';
import { WorkflowComponent } from './components/workflow/workflow.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: ROUTES['workflow'].path,
        pathMatch: 'full',
    },
    {
        path: ROUTES['workflow'].path,
        component: WorkflowComponent
    },
    {
        path: ROUTES['notification'].path,
        component: NotificationComponent
    },
    {
        path: ROUTES['email'].path,
        component: EmailComponent
    },
    {
        path: ROUTES['stand-by'].path,
        component: StandByComponent
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
