import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WorkflowComponent } from './components/workflow/workflow.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {DragDropModule} from "@angular/cdk/drag-drop";
import { NotificationComponent } from './components/notification/notification.component';
import { EmailComponent } from './components/email/email.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {ReactiveFormsModule} from "@angular/forms";
import { ToastrModule } from 'ngx-toastr';
import { StandByComponent } from './components/stand-by/stand-by.component';

@NgModule({
  declarations: [
    AppComponent,
    WorkflowComponent,
    NotificationComponent,
    EmailComponent,
    StandByComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatButtonModule,
        DragDropModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        HttpClientModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(), // ToastrModule added
    ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
