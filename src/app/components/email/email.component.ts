import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { EmailService } from 'src/app/services/email.service';
import { ToastrService } from "ngx-toastr";
import { finalize } from "rxjs";

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})

export class EmailComponent implements OnInit {
    emails_form: FormGroup;
    constructor(private emailService: EmailService, private toastr: ToastrService) { }

    ngOnInit(): void {
        this.setForm();
    }

    public listOfEmailConfigurations() {
        this.emailService.getEmailConfigurations().subscribe(res => {
            console.log(res)
        })
    }

    onCreateEmailConfigurations() {
        this.emailService.createEmailConfigurations(this.emails_form.value).pipe(
            finalize(() => {
                    this.emails_form.reset();
                }
            )
        ).subscribe(
            (response) => {
                this.toastr.success('Email configuré avec sucées')
            }
        )
    }

    setForm() {
        this.emails_form = new FormGroup({
            host: new FormControl('', ),
            port: new FormControl('', ),
            username: new FormControl('', ),
            password: new FormControl('', ),
            method: new FormControl('', ),
            from: new FormControl('', )
        });
    }

}
