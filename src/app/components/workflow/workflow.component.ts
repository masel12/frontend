import {ChangeDetectorRef, Component, OnInit } from '@angular/core';
import {CdkDragDrop, copyArrayItem, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {SaveformService} from "../../services/saveform.service";
import {finalize} from "rxjs";
import { Router } from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-workflow',
    templateUrl: './workflow.component.html',
    styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {

    collections: any

    constructor(
        private fb: FormBuilder,
        private cd: ChangeDetectorRef,
        private http: HttpClient,
        private saveForm: SaveformService,
        private router: Router,
        private toastr: ToastrService
    ) {
    }

    toolsElements = ["Email", "Notification"];
    workflowElements = [];
    workflow: any;
    public myForm: FormGroup = this.fb.group({});

    ngOnInit() {
        //this.displayCounter(this.formControlOptions)
        this.http.get('/assets/workflow.json').subscribe((form) => {
            //console.log(form)
            this.workflow = form;
            console.log(this.workflow)
            this.createForm(this.workflow.inscription.controls);
        })
    }

    createForm(controls) {
        for (const control of controls) {
            const validatorsToAdd = [];

            for (const [key, value] of Object.entries(control.validators)) {
                switch (key) {
                    case 'min':
                        // @ts-ignore
                        validatorsToAdd.push(Validators.min(value));
                        break;
                    case 'max':
                        // @ts-ignore
                        validatorsToAdd.push(Validators.max(value));
                        break;
                    case 'required':
                        if (value) {
                            // @ts-ignore
                            validatorsToAdd.push(Validators.required);
                        }
                        break;
                    case 'requiredTrue':
                        if (value) {
                            // @ts-ignore
                            validatorsToAdd.push(Validators.requiredTrue);
                        }
                        break;
                    case 'email':
                        if (value) {
                            // @ts-ignore
                            validatorsToAdd.push(Validators.email);
                        }
                        break;
                    case 'minLength':
                        // @ts-ignore
                        validatorsToAdd.push(Validators.minLength(value));
                        break;
                    case 'maxLength':
                        // @ts-ignore
                        validatorsToAdd.push(Validators.maxLength(value));
                        break;
                    case 'pattern':
                        // @ts-ignore
                        validatorsToAdd.push(Validators.pattern(value));
                        break;
                    case 'nullValidator':
                        if (value) {
                            // @ts-ignore
                            validatorsToAdd.push(Validators.nullValidator);
                        }
                        break;
                    default:
                        break;
                }
            }

            this.myForm.addControl(
                control.name,
                this.fb.control(control.value, validatorsToAdd)
            );
        }
    }
    onSubmit() {
        console.log('Form valid: ', this.myForm.valid);
        console.log('Form values: ', this.myForm.value);
        if(!this.myForm.valid){
            this.toastr.warning('Form invalid')
            return
        }

        this.saveForm.saveForm(this.myForm.value).pipe(
            finalize(() => {
                   console.log('finalized !!')
                }
            )
        ).subscribe(
            (response) => {
                if(this.getNextKey() == 'keycloak'){
                    this.router.navigate(['stand-by'])
                }

            }
        )
        // TODO: Implement the function that would fill the new JSON file
    }

    getNextKey(){
        let keys = Object.keys(this.workflow);
        let nextKey = keys.indexOf('inscription')+1;
        return keys[nextKey];
    }
}













