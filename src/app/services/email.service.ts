import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { Config } from "../../config";

@Injectable({
    providedIn: 'root'
})
export class EmailService {

    constructor(private http: HttpClient) { }

    public getEmailConfigurations() {
        return this.http.get(Config.api.email.get)
    }

    public createEmailConfigurations(data: any): Observable<any> {
        return this.http.post(Config.api.email.save, data)
    }
}
