import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { Config } from "../../config";

@Injectable({
    providedIn: 'root'
})
export class SaveformService {

    constructor(private http: HttpClient) { }

    public saveForm(data: any): Observable<any> {
        return this.http.post(Config.api.savedForm.create, data)
    }
}
